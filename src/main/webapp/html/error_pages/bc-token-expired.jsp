<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<portlet:defineObjects />
<liferay-theme:defineObjects />
<%String orderData = (String) renderRequest.getAttribute("orderData");%>

<portlet:renderURL var="normalState"
	windowState="<%=LiferayWindowState.NORMAL.toString()%>" />

<p class="lead">
	We're sorry, your order <c:out value="${orderData}"></c:out> has either expired or was already submitted.<br>
	Please close this page and click the Push to VRE button from Blue-Cloud Data Discovery &amp; Access Service again.
</p>


<a class="btn btn-large  btn-primary" href="javascript:window.close();"><i
	class="icon icon-times"></i>&nbsp;Close</a>

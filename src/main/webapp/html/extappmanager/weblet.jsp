<%@include file="/html/init.jsp"%>


<%@ page import="java.util.Base64"%>

<%
String encodedD4sBootDIV = (String) request.getAttribute("d4s-boot-div");
String encodedExtAppDIV = (String) request.getAttribute("ext-app-div");

if (encodedD4sBootDIV == null || encodedExtAppDIV == null) {
	String d4sBootDIV = new String(
	"Configuration is missing, please enter boot, manager urls and clientId name in portlet configuration");
} else {
	byte[] decodedBytes = Base64.getDecoder().decode(encodedD4sBootDIV);
	String d4sBootDIV = new String(decodedBytes);

	decodedBytes = Base64.getDecoder().decode(encodedExtAppDIV);
	String extAppDIV = new String(decodedBytes);

	pageContext.setAttribute("d4sBootDIV", d4sBootDIV);
	pageContext.setAttribute("extAppDIV", extAppDIV);
}
%>
${d4sBootDIV}

<div id="extAppDIV">${extAppDIV}</div>

<%@include file="/html/init.jsp"%>

<liferay-portlet:actionURL portletConfiguration="true"
	var="configurationURL" />

<%
String bootURL_cfg = GetterUtil.getString(portletPreferences.getValue("bootURL", StringPool.BLANK));
String managerURLcfg = GetterUtil.getString(portletPreferences.getValue("managerURL", StringPool.BLANK));
String iamClientId_cfg = GetterUtil.getString(portletPreferences.getValue("iamClientId", StringPool.BLANK));
%>

<aui:form action="<%=configurationURL%>" method="post" name="fm">
	<aui:input name="<%=Constants.CMD%>" type="hidden"
		value="<%=Constants.UPDATE%>" />

	<!-- Boot URL -->
	<aui:field-wrapper cssClass="field-group">
		<aui:input style="width: 100%;" name="preferences--bootURL--"
			type="text" cssClass="text long-field" showRequiredLabel="true"
			label="D4Science Visuals CDN Boot URL" inlineField="true" inlineLabel="left"
			placeholder="e.g. https://cdn.d4science.org/visuals/d4s-cdn/d4s-boot"
			helpMessage="Actual endpoint of the cdn visuals on D4Science"
			value="<%=bootURL_cfg%>" required="true" />
	</aui:field-wrapper>
		<!-- manager URL -->
	<aui:field-wrapper cssClass="field-group">
		<aui:input style="width: 100%;" name="preferences--managerURL--"
			type="text" cssClass="text long-field" showRequiredLabel="true"
			label="D4Science Visuals CDN Manager URL" inlineField="true" inlineLabel="left"
			placeholder="e.g. https://cdn.d4science.org/visuals/d4s-vre-manager/ext-app-manager"
			helpMessage="Actual endpoint of the cdn visuals Manager on D4Science"
			value="<%=managerURLcfg%>" required="true" />
	</aui:field-wrapper>
		<!-- IAM Client ID (App Name) -->
	<aui:field-wrapper cssClass="field-group">
		<aui:input style="width: 100%;" name="preferences--iamClientId--"
			type="text" cssClass="text long-field" showRequiredLabel="true"
			label="IAM Client Id" inlineField="true" inlineLabel="left"
			placeholder="enter the IAM ClientId of the App here"
			helpMessage="Actual IAM Client Id for this app"
			value="<%=iamClientId_cfg%>" required="true" />
	</aui:field-wrapper>
	
	<aui:button-row>
		<aui:button type="submit" />
	</aui:button-row>

</aui:form>
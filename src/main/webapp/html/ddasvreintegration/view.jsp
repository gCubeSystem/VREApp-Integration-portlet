<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@page import="com.liferay.portal.model.Layout"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/security"
	prefix="liferay-security"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />
<%
Boolean responseResultSuccess = (Boolean) renderRequest.getAttribute("theResult");
String orderData = (String) renderRequest.getAttribute("orderData");

pageContext.setAttribute("orderData", orderData);
pageContext.setAttribute("responseResultSuccess", responseResultSuccess);
%>
<c:choose>

	<c:when test="${not responseResultSuccess}">
		<p class="lead">
			Ouch, we're sorry an error occurred. Please report the issue on <a
				href="https://support.d4science.org">https://support.d4science.org</a>
		</p>
	</c:when>

	<c:otherwise>
		<p class="lead">Your order <c:out value="${orderData}"></c:out>, is being retrieved and the dataset are being saved to your
			workspace area, you will receive an e-email when the
			download process is completed. You may close this Tab now.</p>
			<a class="btn btn-large  btn-primary" href="javascript:window.close();"><i
	class="icon icon-times"></i>&nbsp;Close</a>
	</c:otherwise>
</c:choose>

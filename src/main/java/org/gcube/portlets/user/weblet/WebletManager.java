package org.gcube.portlets.user.weblet;

import java.io.IOException;
import java.util.Base64;

import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.gcube.common.portal.PortalContext;
import org.gcube.oidc.rest.JWTToken;
import org.gcube.portal.oidc.lr62.OIDCUmaUtil;

import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import okhttp3.*;

/**
 * Portlet implementation class WebletManager
 */
public class WebletManager extends MVCPortlet {
	private static com.liferay.portal.kernel.log.Log _log = LogFactoryUtil.getLog(WebletManager.class);
	//final String D4S_BOOT_URL = "https://cdn.dev.d4science.org/visuals/d4s-cdn/d4s-boot";
	//final String EXT_APP_MANAGER_URL = "https://cdn.dev.d4science.org/visuals/d4s-vre-manager/ext-app-manager?app=_myextapp";


	public static final String D4S_BOOT_ATTR = "d4s-boot-div";
	public static final String EXT_APP_MANAGERATTR = "ext-app-div";


	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {

		PortletPreferences portletPreferences = renderRequest.getPreferences();
		String D4S_BOOT_URL = GetterUtil.getString(portletPreferences.getValue("bootURL", StringPool.BLANK));
		String WEBLET_MANAGER_URL = GetterUtil.getString(portletPreferences.getValue("managerURL", StringPool.BLANK));
		String IAM_CLIENTID = GetterUtil.getString(portletPreferences.getValue("iamClientId", StringPool.BLANK));

		String webletForIamClientURL = new StringBuilder(WEBLET_MANAGER_URL).append("?app=").append(IAM_CLIENTID).toString();

		if (D4S_BOOT_URL.equals(StringPool.BLANK) || WEBLET_MANAGER_URL.equals(StringPool.BLANK) || IAM_CLIENTID.equals(StringPool.BLANK)) {
			_log.warn("Missing parameters in config");
		}
		else {
			JWTToken umaToken = null;
			try {
				String username = PortalUtil.getUser(renderRequest).getScreenName();
				HttpServletRequest httpReq = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
				String context = getCurrentContext(renderRequest);
				umaToken = OIDCUmaUtil.getUMAToken(httpReq, username, context);
			} catch (Exception e) {
				e.printStackTrace();
			}
			String token =  umaToken.getAccessTokenString();


			OkHttpClient client = new OkHttpClient();
			Request request = new Request.Builder()
					.url(D4S_BOOT_URL)
					.addHeader("cache-control", "no-cache")
					.addHeader("Authorization", "Bearer " + token)
					.build();
			Call call = client.newCall(request);
			Response response = call.execute();
			String d4sBootDIV = response.body().string();
			String encodedSd4sBootDIV = Base64.getEncoder().encodeToString(d4sBootDIV.getBytes());
			renderRequest.setAttribute(D4S_BOOT_ATTR, encodedSd4sBootDIV);
			request = new Request.Builder()
					.url(webletForIamClientURL)
					.addHeader("cache-control", "no-cache")
					.addHeader("Authorization", "Bearer " + token)
					.build();
			call = client.newCall(request);
			response = call.execute();
			String extAppDIV = response.body().string();
			String encodedextAppDIV = Base64.getEncoder().encodeToString(extAppDIV.getBytes());	
			renderRequest.setAttribute(EXT_APP_MANAGERATTR, encodedextAppDIV);
		}
		super.render(renderRequest, renderResponse);		
	}

	public static String getCurrentContext(RenderRequest request) {
		long groupId = -1;
		try {
			groupId = PortalUtil.getScopeGroupId(request);
			return getCurrentContext(groupId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getCurrentContext(long groupId) {
		try {
			PortalContext pContext = PortalContext.getConfiguration(); 
			return pContext.getCurrentScope(""+groupId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}

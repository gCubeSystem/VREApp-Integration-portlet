package org.gcube.portlets.user.weblet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;

public class WebletConfigurationAction extends DefaultConfigurationAction {
	private static Log _log = LogFactoryUtil.getLog(WebletConfigurationAction.class);
    @Override
    public void processAction(
        PortletConfig portletConfig, ActionRequest actionRequest,
        ActionResponse actionResponse) throws Exception {  

        super.processAction(portletConfig, actionRequest, actionResponse);

        PortletPreferences prefs = actionRequest.getPreferences();
        String bootURL = prefs.getValue("bootURL", "true");
        String managerURL = prefs.getValue("managerURL", "true");
        String iamClientId = prefs.getValue("iamClientId", "true");
    
        _log.debug("bootURL = " + bootURL + " in PublicWebappConfigurationAction.processAction().");
        _log.debug("managerURL = " + managerURL + " in PublicWebappConfigurationAction.processAction().");
        _log.debug("iamClientId = " + iamClientId + " in PublicWebappConfigurationAction.processAction().");
    }
    
    @Override
    public String render(PortletConfig portletConfig,
            RenderRequest renderRequest, RenderResponse renderResponse)
            throws Exception {
 
        return "/html/extappmanager/config.jsp";
    }
 
}
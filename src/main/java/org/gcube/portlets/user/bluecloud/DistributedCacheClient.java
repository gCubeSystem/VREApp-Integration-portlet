package org.gcube.portlets.user.bluecloud;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

import org.gcube.common.portal.PortalContext;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.resources.gcore.utils.Group;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.slf4j.LoggerFactory;

import net.spy.memcached.KetamaConnectionFactory;
import net.spy.memcached.MemcachedClient;

/**
 * @author Massimiliano Assante at ISTI-CNR 
 */
public class DistributedCacheClient {

	// Logger
	private static final org.slf4j.Logger _log = LoggerFactory.getLogger(DistributedCacheClient.class);

	private static final String MEMCACHED_RESOURCE_NAME = "Memcached";
	private static final String CATEGORY = "Database";

	private MemcachedClient mClient;

	/**
	 * Build the singleton instance
	 */
	public DistributedCacheClient(){ 
		List<InetSocketAddress> addrs = discoverMemcachedClusterEndpoint();
		try {
			mClient =  new MemcachedClient(new KetamaConnectionFactory(), addrs);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	

	public MemcachedClient getMemcachedClient() {
		return mClient;
	}

	/**
	 * Retrieve endpoint resoruce from IS 
	 * @return List of InetSocketAddresses 
	 * @throws Exception
	 */
	private static List<InetSocketAddress> discoverMemcachedClusterEndpoint(){

		String currentScope = ScopeProvider.instance.get();
		String infrastructure = "/"+PortalContext.getConfiguration().getInfrastructureName();
		ScopeProvider.instance.set(infrastructure);
		List<InetSocketAddress> toReturn = new ArrayList<InetSocketAddress>();
		try{
			SimpleQuery query = queryFor(ServiceEndpoint.class);
			query.addCondition("$resource/Profile/Name/text() eq '"+ MEMCACHED_RESOURCE_NAME +"'");
			query.addCondition("$resource/Profile/Category/text() eq '"+ CATEGORY +"'");
			DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);
			List<ServiceEndpoint> ses = client.submit(query);
			if (ses.isEmpty()) {
				_log.error("There is no Memcached cluster having name: " + MEMCACHED_RESOURCE_NAME + " and Category " + CATEGORY + " on root context in this infrastructure: ");
				return null;
			}
			for (ServiceEndpoint se : ses) {
				Group<AccessPoint> aps = se.profile().accessPoints();
				for (AccessPoint ap : aps.asCollection()) {
					String address = ap.address();  //e.g. node-d-d4s.d4science.org:11211
					String[] splits = address.split(":");
					String hostname = splits[0];
					int port = Integer.parseInt(splits[1]);
					toReturn.add(new InetSocketAddress(hostname, port));
				}				
				break;
			}
		} catch(Exception e){
			_log.error("Error while retrieving hosts for the Memcached cluster having name: " + MEMCACHED_RESOURCE_NAME + " and Category " + CATEGORY + " on root context");
		}finally{
			ScopeProvider.instance.set(currentScope);
		}
		ScopeProvider.instance.set(currentScope);
		return toReturn;
	}

}

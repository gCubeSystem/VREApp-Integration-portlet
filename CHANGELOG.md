
# Changelog for VRE App Integration Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.2.1-SNAPSHOT] - 2022-07-13

 - Weblet manager is now instanciable (can appear more than once on a page) 

## [v1.2.0] - 2022-02-18

- Added Weblet support

## [v1.1.0] - 2021-07-13

- Ported to git


## [v1.0.0] - 2019-05-21

First Release
